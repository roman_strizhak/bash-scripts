# README #

#### How to run bash (.sh) script 
* Execute:
```
sh ./directory/script.sh
```

#### How to run bash (.sh) script in IntelliJ IDEA:
* Install BashSupport plugin
* Menu -> Run -> "Edit Configurations..." -> "Add New Configuration" -> "Bash".Parameters:
- Script: {path\script.sh}  (example -
 `D:\WorkSpace\bash_scripts\src\main\resources\scripts\pos_precondition_for_parallel.sh`)
- Interpreter path: {path\bash.exe} (example - `C:\Program Files\Git\bin\bash.exe`)
- Working directory: {project_name\} (example - `D:\WorkSpace\bash_scripts\`)
- Environment variables: (example - `CONFIG_FILE=loclhost.properties`)