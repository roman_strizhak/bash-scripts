#!/bin/bash

function info() {
    >&2 echo "[info] $@"
}

function error() {
    >&2 echo "[ERROR] $@"
}

function request() {
    info "curl -s -H \"Content-Type: application/json\" $@"
    curl -s -H "Content-Type: application/json" "$@"
}

function getRandomAlphaNumericString() {
    echo $(cat /dev/urandom | env LC_CTYPE=C tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
}

#----

if [[ -z "${1}" ]]; then
    error "Subreddit name is undefined! Example: funnyvideos"
    exit 1;
else
    SUBREDDIT_NAME=${1}
    SUBREDDIT_JSON_URL="https://www.reddit.com/r/${SUBREDDIT_NAME}/new.json"
fi

echo "--->>> [1] REMOVE /${SUBREDDIT_NAME} FROM CURRENT DIRECTORY"
rm -rf ${SUBREDDIT_NAME}

echo "--->>> [2] GET SUBREDDIT JSON"
RANDOM_USER_AGENT=`getRandomAlphaNumericString`
UNESCAPED_SUBREDDIT_JSON=`request -H "User-agent: ${RANDOM_USER_AGENT}" -X GET "${SUBREDDIT_JSON_URL}"`
SUBREDDIT_JSON=`echo ${UNESCAPED_SUBREDDIT_JSON} | sed -e 's/\\\n/_/g' | sed -e 's/\\\t/_/g' | sed -e 's/\\\"/_/g'`

echo "--->>> [3] START TO COLLECT SUBREDDIT MEDIA FILES FROM THE LAST POSTS"
mkdir ${SUBREDDIT_NAME}
POSTS_COUNT=`echo ${SUBREDDIT_JSON} | jq '.data .children | length'`;
info "found ${POSTS_COUNT} posts"

for (( i = 1; i <= ${POSTS_COUNT}; i ++ )); do
    POST_TITLE=`echo ${SUBREDDIT_JSON} | jq '.data .children['$((i - 1))'] .data .title'`
    POST_MEDIA_URL=`echo ${SUBREDDIT_JSON} | jq -r '.data .children['$((i - 1))'] .data .url'`

    # download media file from post
    echo "--->>> [3.${i}] DOWNLOAD '${POST_MEDIA_URL}' MEDIA FILE FROM ${POST_TITLE} POST ${i}"
    MEDIA_FILE_NAME=${SUBREDDIT_NAME}/reddit_media_${i}_`getRandomAlphaNumericString`.mp4
#     youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' -o ${MEDIA_FILE_NAME} ${POST_MEDIA_URL}

    # write result to file
    MEDIA_FILE_PATH=`pwd`"/${MEDIA_FILE_NAME}"
    echo "title: ${POST_TITLE}" >> ${SUBREDDIT_NAME}/output.txt
    echo "url: ${POST_MEDIA_URL}" >> ${SUBREDDIT_NAME}/output.txt
#     echo "path: ${MEDIA_FILE_PATH}\n" >> ${SUBREDDIT_NAME}/output.txt
done
