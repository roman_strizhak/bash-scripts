#!/bin/bash

ENV_PROPERTY_FILES_PATH="src/main/resources/environments"
ENV_PROPERTY_FILE="${ENV_PROPERTY_FILES_PATH}/${ENV_CONFIG_FILE}"

function info() {
    >&2 echo "[INFO] ${FUNCNAME[1]}(): $@"
}

function error() {
    >&2 echo "[ERROR] ${FUNCNAME[1]}(): $@"
}

function request() {
    info "curl -sSf $@"
    curl -sSf "$@"
}

function getEnvParameterValue {
    local PARAMETER_NAME="$1"

    echo $(cat ${ENV_PROPERTY_FILE} | grep ${PARAMETER_NAME} | cut -d '=' -f 2 | tr -d '[[:space:]]')
}

function checkEnvHosts() {
    local ENV_HOSTS="$1";
    info ${ENV_HOSTS}
    for HOST in ${ENV_HOSTS}; do
        RESPONSE=`request "${HOST}" -o /dev/null; echo $?`
        if [ ${RESPONSE} != 0 ]; then
            ERROR_MESSAGE="environment hosts verification FAILED!"
        fi
    done

    if [[ -z "${ERROR_MESSAGE}" ]]; then
        info "SUCCESS -> all environment hosts are available."
    else
        error "${ERROR_MESSAGE}"
        exit 1;
    fi
}

AUTH_HOST=`getEnvParameterValue auth.site.host`
SOFARM_HOST=`getEnvParameterValue sofarm.site.host`
SFPAGES_HOST=`getEnvParameterValue sfpages.site.host`
BILLING_HOST=`getEnvParameterValue billing.site.host`

ENV_HOSTS="${AUTH_HOST} ${SOFARM_HOST} ${SFPAGES_HOST} ${BILLING_HOST}";
checkEnvHosts "${ENV_HOSTS}" || exit 1;