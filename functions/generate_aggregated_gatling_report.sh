#!/bin/bash

INDEX_HTML="<html><head><title>Gatling reports</title></head><body style='background-color:#f0f5fb'>"
INDEX_HTML="${INDEX_HTML}<h1 style='color:grey'>Gatling reports:</h1>"
REPORT_DIRS=$(ls target/gatling/ | tr " " "\n")
for REPORT_DIR in $REPORT_DIRS; do
  REPORT_NAME=$(echo "${REPORT_DIR}" | cut -f1 -d-)
  FAILED_RUN=$(cat "target/gatling/${REPORT_DIR}/index.html" | grep "ko total" | wc -l)
  if [ "${FAILED_RUN}" != "0" ]; then
    EXECUTION_STATUS="<span style='color:red;font-size:25px'>(FAILED) </span>"
  else
    EXECUTION_STATUS="<span style='color:green;font-size:25px'>(SUCCESS) </span>"
  fi
  INDEX_HTML="${INDEX_HTML}${EXECUTION_STATUS}<a style='color:orange;font-size:30px' href='${REPORT_DIR}/index.html'>${REPORT_NAME}</a><br><br>"
done
INDEX_HTML="${INDEX_HTML}</body></html>"
echo "$INDEX_HTML" > target/gatling/index.html
echo "Aggregated Gatling report was generated at" "$(pwd)/"target/gatling/index.html
