#!/bin/bash

if [[ -z "${WORKSPACE}" ]]; then
    echo "Empty WORKSPACE variable:" ${WORKSPACE}
    exit 1;
fi

function info() {
    >&2 echo "[INFO] ${FUNCNAME[1]}(): $@"
}

CURRENT_DATE=$(date +%d-%m-%Y);

HTML_REPORT="<!DOCTYPE html><html><head><title>Failed Scenarios</title></head>";
HTML_REPORT="${HTML_REPORT}<body style='background-color:#f7f8f3;'><center><table border=1>";
HTML_REPORT="${HTML_REPORT}<thead><tr><th width="10%">date</th><th width="45%">scenario name</th><th width="45%">error message</th></tr></thead>";

FEATURE_FILES_COUNT=$((`cat ${WORKSPACE}/target/cucumber.json | jq -r '. | length'` - 1));
FEATURE_FILES_SEQ=$(seq 0 ${FEATURE_FILES_COUNT});

info "FAILED SCENARIOS:"
for feature in ${FEATURE_FILES_SEQ}; do
    SCENARIOS_COUNT=$((`cat ${WORKSPACE}/target/cucumber.json | jq -r '.['${feature}'] | .elements | length'` - 1));
    SCENARIOS_SEQ=$(seq 0 ${SCENARIOS_COUNT});

    for scenario in ${SCENARIOS_SEQ}; do
        STEPS_COUNT=$((`cat ${WORKSPACE}/target/cucumber.json | jq -r '.['${feature}'] | .elements | .['${scenario}'].steps | length'` - 1));
        STEPS_SEQ=$(seq 0 ${STEPS_COUNT});

        for step in ${STEPS_SEQ}; do
            STEP_STATUS=`cat ${WORKSPACE}/target/cucumber.json | jq -r '.['${feature}'] | .elements | .['${scenario}'].steps | .['${step}'].result.status'`;
            if [[ "${STEP_STATUS}" == "failed" ]]; then
                SCENARIO_NAME=`cat ${WORKSPACE}/target/cucumber.json | jq -r '.['${feature}'] | .elements | .['${scenario}'].name'`;
                SCENARIO_NAME="${SCENARIO_NAME} [${scenario}]"
                ERROR_MESSAGE=`cat ${WORKSPACE}/target/cucumber.json | jq -r '.['${feature}'] | .elements | .['${scenario}'].steps | .['${step}'].result.error_message'`;

                FORMATTED_ERROR_MESSAGE="";
                if [[ ${ERROR_MESSAGE} == *"AssertionError"* ]]; then
                    FORMATTED_ERROR_MESSAGE=`echo ${ERROR_MESSAGE} | sed -e 's/ at .*//g'`;
                else
                    ERROR_MESSAGE=`echo "${ERROR_MESSAGE}" | tr "\t" "|"`;
                    IFS='|' read -ra values <<<"${ERROR_MESSAGE}";
                    FORMATTED_ERROR_MESSAGE=${values[0]};
                    FORMATTED_ERROR_MESSAGE=${FORMATTED_ERROR_MESSAGE#"net.serenitybdd.core.exceptions.SerenityManagedException: The following error occurred:"};
                fi

                FORMATTED_ERROR_MESSAGE=`echo ${FORMATTED_ERROR_MESSAGE} | sed -e 's/&/\&amp;/g; s/</\&lt;/g; s/>/\&gt;/g; s/"/\&quot;/g; s/'"'"'/\&#39;/g'`;

                info ${SCENARIO_NAME};
                HTML_REPORT="${HTML_REPORT}<tr><td>${CURRENT_DATE}</td><td>${SCENARIO_NAME}</td><td>${FORMATTED_ERROR_MESSAGE}</td></tr>";
            fi
        done
    done
done

HTML_REPORT="${HTML_REPORT}</table></center></body></html>";
echo ${HTML_REPORT} > ${WORKSPACE}/target/site/serenity/fails_report.html;
sed -i 's/Requirements/Failed scenarios/g' ${WORKSPACE}/target/site/serenity/index.html;
sed -i 's/capabilities.html/fails_report.html/g' ${WORKSPACE}/target/site/serenity/index.html;