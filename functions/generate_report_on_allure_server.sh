#!/bin/bash

ALLURE_DIR=allure-results

# init environment.properties with defined variables
echo "CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA}" >> ${ALLURE_DIR}/environment.properties
echo "CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH}" >> ${ALLURE_DIR}/environment.properties
echo "CI_PIPELINE_INITIATOR=${GITLAB_USER_NAME}" >> ${ALLURE_DIR}/environment.properties

# zip allure results files to archive at current directory
ARCHIVE_NAME=${ALLURE_DIR}.zip
zip -jq ${ARCHIVE_NAME} ${ALLURE_DIR}/*.json ${ALLURE_DIR}/environment.properties

# upload archive with results to Allure Portal
UPLOAD_RESPONSE=$(curl -s -X POST 'http://'${ALLURE_PORTAL_HOST}':'${ALLURE_PORTAL_PORT}'/api/result' \
-H 'Content-Type:multipart/form-data' \
-F 'allureResults=@'${CI_PROJECT_DIR}'/'${ARCHIVE_NAME}';type=application/zip')
ALLURE_RESULTS_UUID=$(echo ${UPLOAD_RESPONSE} | jq '.uuid' | tr -d '"')

# generate Allure Report
NEW_REPORT_BODY='{"reportSpec":{"path":["branch","'${CI_COMMIT_BRANCH}'"],"executorInfo":{"name":"'${CI_RUNNER_DESCRIPTION}'","buildName":"Pipeline:#'${CI_PIPELINE_ID}'","buildUrl":"'${CI_PIPELINE_URL}'","reportName":"tests"}},"results":["'${ALLURE_RESULTS_UUID}'"],"deleteResults":true}'
GENERATE_RESPONSE=$(curl -s -X POST 'http://'${ALLURE_PORTAL_HOST}':'${ALLURE_PORTAL_PORT}'/api/report' \
-H 'Content-Type:application/json' \
-d ${NEW_REPORT_BODY})
ALLURE_REPORT_URL=$(echo ${GENERATE_RESPONSE} | jq '.url' | tr -d '"')

# output Allure Report url using blue text
echo "$(tput -Txterm setaf 4)Allure Report: ${ALLURE_REPORT_URL}" && tput -Txterm sgr 0 > /dev/null