#!/bin/bash


echo "DBUS_SESSION_BUS_ADDRESS=/dev/null"

# launch Xvfb display
Xvfb -ac :${JOB_NAME:0:2} -screen 0 1920x1080x16 &
echo "DISPLAY=:${JOB_NAME:0:2}"

# TEST RUN

# kill Xvfb display
pkill -f Xvfb.*${DISPLAY}

# join cucu


mber json files
cd ${WORKSPACE}/target
jq -s '[.[][]]' *.json > cucumber.json

# generate fails report
bash ${WORKSPACE}/src/main/resources/scripts/generate_fails_report.sh

# remove redundant serenity files
cd ${WORKSPACE}/target/site/serenity
rm -rf *.{json,csv,xml,txt}

# remove temp files
cd /tmp
sudo rm -rf .com.google.Chrome.*
sudo rm -rf .org.chromium.Chromium.*

#
# execute TAF bash script
if [[ -z "${ENVIRONMENT}" ]]; then echo "[ERROR] Empty {ENVIRONMENT} variable!"; exit 1; fi
if [[ -z "${BASH_SCRIPT}" ]]; then echo "[ERROR] Empty {BASH_SCRIPT} variable!"; exit 1; fi
export ENV_CONFIG_FILE=${ENVIRONMENT}
bash ./src/main/resources/scripts/${BASH_SCRIPT} || exit 1;

# change private key permissions
cd ${WORKSPACE}/src/main/resources/sshkeys/
chmod 400 key_name

# if success
exit 0
