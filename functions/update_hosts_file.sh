#!/bin/bash

# Adding or Removing items to/from hosts file

function info() {
    >&2 echo "[INFO] ${FUNCNAME[1]}(): $@"
}

function getHostsFile() {
    echo $(cat /etc/hosts)
}

ACTION=$1
HOST_NAME=$2
DEFAULT_IP=127.0.0.1
IP=${3:-$DEFAULT_IP}

case "${ACTION}" in
    add)
        if [[ "`getHostsFile`" != *"${HOST_NAME}"* ]]; then
            sudo -S -- sh -c "echo ${IP} ${HOST_NAME} >> /etc/hosts"
            info "${IP} ${HOST_NAME} was added to /etc/hosts"
        else
            info "${HOST_NAME} already exist in /etc/hosts"
        fi
    ;;

    remove)
        sudo -S sed -ie "\|^${IP} ${HOST_NAME}\$|d" /etc/hosts
        info "${IP} ${HOST_NAME} was removed from /etc/hosts"
    ;;

    *)
        echo "Usage: "
        echo "update_hosts_file.sh [add|remove] [hostname] [ip]"
        echo "Default IP is 127.0.0.1"
        echo
        echo "Examples:"
        echo "update_hosts_file.sh add test.com"
        echo "update_hosts_file.sh remove test.com 192.168.1.1"
        exit 1
    ;;
esac

info "`getHostsFile`"

exit 0