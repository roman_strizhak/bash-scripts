#!/usr/bin/env bash

currentDir="$(dirname "$0")"
utilsDir="${currentDir/functions/utils}"
source "${utilsDir}"/logger.sh;

function restartSkypeBot(){
    info "======================================="
    info "Restart skype bot on the remote host"
    info "======================================="

    sshpass -p 'remoteUserPassword' ssh -tt -o StrictHostKeyChecking=no remote_user@host.name << EOF

    cd /home/remote_user/skypebot
    kill $(ps aux | grep -m 1 'skype4jenkins' | awk '{print $2}')
    ./run_bot.sh
    sleep 10
    tail reports/console/trace.log
    exit
EOF
}

exit 0;