#!/usr/bin/env bash

currentDir="$(dirname "$0")"
utilsDir="${currentDir/functions/utils}"
source "${utilsDir}"/logger.sh;
source "${utilsDir}"/property_parser.sh

PROPERTY_FILE="properties/environment.properties"
DB_KEY="mysql"
MYSQL_HOST=`get_parameter_from_config ${PROPERTY_FILE} ${DB_KEY}.hostname`
MYSQL_PORT=`get_parameter_from_config ${PROPERTY_FILE} ${DB_KEY}.port`
MYSQL_DB_NAME=`get_parameter_from_config ${PROPERTY_FILE} ${DB_KEY}.db.name`
MYSQL_USER=`get_parameter_from_config ${PROPERTY_FILE} ${DB_KEY}.username`
MYSQL_PASSWORD=`get_parameter_from_config ${PROPERTY_FILE} ${DB_KEY}.password`

function updateUser(){
    local user_id="$1"

    UPDATE_USER_QUERY=$(
	cat <<-EOF
		UPDATE user_table
		SET user_first_name='Bob', user_last_name = 'Marley', active_flag = 1
		WHERE user_id='$user_id';
	EOF
    )

	info "SQL query: \n${UPDATE_USER_QUERY}\n"

    mysql --host=${MYSQL_HOST} --port=${MYSQL_PORT} \
	      --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} \
	      --database=${MYSQL_DB_NAME} --execute="${UPDATE_USER_QUERY}"
}

updateUser 123456

exit 0;