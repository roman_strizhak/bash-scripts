#!/usr/bin/env bash

currentDir="$(dirname "$0")"
utilsDir="${currentDir/functions/utils}"
source "${utilsDir}"/logger.sh;
source "${utilsDir}"/api_util.sh;

RESPONSE=`request -X GET http://api.fixer.io/latest`
info $RESPONSE

RESPONSE=`request -X POST -d '{ "login": '${LOGIN}', "password": "'${PASSWORD}'" }' http://api.fixer.io/login`
info $RESPONSE

RESPONSE=`request -H "App-Token: ${TOKEN}" -X PUT http://api.fixer.io/latest?base=EUR`
info $RESPONSE

RESPONSE=`request -H "App-Token: ${TOKEN}" -X PATCH http://api.fixer.io/latest/$REASON_CODE/$STATE`
info $RESPONSE

RESPONSE=`request -X DELETE http://api.fixer.io/user/$USER_ID`
info $RESPONSE


exit 0;