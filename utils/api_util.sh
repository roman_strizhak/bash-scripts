#!/usr/bin/env bash

currentDir="$(dirname "$0")"
utilsDir="${currentDir/functions/utils}"
source "${utilsDir}"/logger.sh;

function request(){
    info "Request: curl --insecure --tlsv1.2 -s -H \"Content-Type: application/json\" $@"
    curl --insecure --tlsv1.2 -s -H "Content-Type: application/json" "$@"
}

function silentRequest(){
    curl --insecure --silent "$@"
}

function extractJsonValue() {
    local JSON="$1"
    local KEY="$2"
    VALUE=`echo ${JSON} | grep -oE ''${KEY}'"\W*:\W*"([-A-Za-z0-9\;]*)' | cut -d: -f2 | tr -d '"'`;
    echo $VALUE
}

function extractJsonValues(){
    local JSON="$1 | tr -d '{' | tr -d '}'"

    IFS=',' read -ra values <<< "$JSON"
    echo $values
}

function extractJsonValuesToFile(){
    local JSON="$1 | tr -d '{' | tr -d '}'"

    IFS=',' read -ra values <<< "$JSON"
    echo $values > testedBuildNumber.txt;
}