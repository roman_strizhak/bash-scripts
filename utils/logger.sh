#!/usr/bin/env bash

function info(){
    >&2 echo -e "[INFO] ${FUNCNAME[1]}(): $@"
}

function err(){
    >&2 echo -e "[ERROR] ${FUNCNAME[1]}(): $@"
}

function warn(){
    >&2 echo -e "[WARNING] ${FUNCNAME[1]}(): $@"
}