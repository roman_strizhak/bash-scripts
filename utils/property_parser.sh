#!/usr/bin/env bash

currentDir="$(dirname "$0")"
utilsDir="${currentDir/functions/utils}"
source "${utilsDir}"/logger.sh;

function get_parameter_from_config {
	local config_file="$1"
	local parameter_name="$2"

	local parameter_value=$(cat $config_file | grep "${parameter_name}" | cut -d '=' -f 2 | tr -d '[[:space:]]')
	info "parsed value '$parameter_value' by '$parameter_name' key from $config_file file"
	echo $parameter_value
}